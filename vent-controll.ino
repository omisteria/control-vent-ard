#include <EEPROM.h>
#include <RBD_Timer.h>
#include <OneWire.h>
#include <DS18B20.h>
#include "ModbusRTUSlave.h" //https://github.com/lucasso/ModbusRTUSlaveArduino

/**
 * Data register
 */
#define DATA_SIZE 26
/**
 * Data types in _D array (indexe, type, description)
 * 1 (byte) - id of modbus device - default 11
 * 2 (byte) - speed hi - default 37 (9600 baude)
 * 3 (byte) - speed low - default - 128 (9600 baude)
 * 4 - reserv
 * 5 - reserv
 * 6 - reserv
 * 7 - reserv
 * 8 - reserv
 * 9 - reserv
 * 10 (int *10) - desirable temperature of air (low)
 * 11 (int *10) - desirable temperature of air (hi)
 * 12 (int *10) - current temperature of air (low)
 * 13 (int *10) - current temperature of air (hi)
 * 14 (int *10) - gisterezis for air temperature (low)
 * 15 (int *10) - gisterezis for air temperature (hi)
 * 16 (byte) - air stream intensity (0-100)   
 * 17 (byte) - base min time impulse - default 5
 * 18 (byte) - state of fan
 * 19 (byte) - state of heater
 * 20 (byte) - enabled fan
 * 21 (byte) - enabled heater
 * 22 (int) - period (seconds) freezing for fan (<0 - time of permament disabling, >0 - time of permanent enabling)
 * 23 (byte) - restore state fan & heater from memory
 */

// _D indexes
#define I_MODBUS_DEVICE_ID 11
#define I_MODBUS_SPEED 2
#define I_AIR_DESIRABLE_TEMP 11
#define I_AIR_CURRENT_TEMP 13
#define I_AIR_TEMP_GISTEREZIS 15
#define I_AIR_INTENSITY 17
#define I_AIR_BASE_IMPULSE 19
#define I_STATE_FAN 20
#define I_STATE_HEATER 21
#define I_ENABLED_FAN 22
#define I_ENABLED_HEATER 23
#define I_PERIOD_FREEZE 25
#define I_RESTORE_STATE 26

u16 _D[DATA_SIZE] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0};
u16 _DC[DATA_SIZE];

struct WorkDataStruct {
  int modbusID;
  int modbusSpeed;
  int airDesirableTemp;
  int airCurrentTemp;
  int gisterezisAirTemp;
  int airIntensity;
  int airBaseImpulse;
  int stateFan;
  int stateHeater;
  int enabledFan;
  int enabledHeater;
  int periodFreezeFan;
  int restoreStateFromMem;
};

volatile WorkDataStruct WD;

/**
 * Temperature sensor & heater
 */
#define PIN_TEMPERATURE_SENSOR 2
#define PIN_HEATER_RELAY A0
int pollTemperatureSensor = 2000; // 2 secs
RBD::Timer timerFetchTemperature;
DS18B20 ds(PIN_TEMPERATURE_SENSOR);

/**
 * Fan
 */
#define PIN_FAN_RELAY A1
int fanWorkTime, fanPauseTime; 
RBD::Timer timerFanControl;
RBD::Timer timerFreezeFanControl;

/**
 * Communication (rs485)
 */
byte DEVICE_ID = 11;
#define VIRTUAL_ADDRESS 0x0000 // address (kind of name) of above data, may be anything:
#define PIN_CONNECTED_TO_BOTH_DE_AND_RE 7

/**
 * Memmory
 */
#define VER_MEM_ADDR 0 // address of version
#define DTA_MEM_ADDR 3 // address of data structure
byte versionMem = 3;      // version of structure of memory

struct MemDataStruct {
  byte modbusID;
  int airDesirableTemp;
  int gisterezisAirTemp;
  byte airIntensity;
  int airBaseImpulse;
  byte enabledFan;
  byte enabledHeater;
  byte restoreStateFromMem;
};

MemDataStruct MemD;
RBD::Timer timerSaveToMemmory;

//ModbusRTUSlave rtu = ModbusRTUSlave(WD.modbusID, &Serial1, PIN_CONNECTED_TO_BOTH_DE_AND_RE);
ModbusRTUSlave modbus(&Serial1, PIN_CONNECTED_TO_BOTH_DE_AND_RE);

 
void setup() {
  Serial.begin(115200);

  // default initialization
  setModbusDeviceId(11);
  setModbusSpeed(9600);
  setAirDesirableTemp(160); // 16.0 C
  setGisterezisAirTemp(10); // 1.0 C
  WD.periodFreezeFan = 0;
  WD.restoreStateFromMem = 1;
  setEnabledHeater(1);
  setEnabledFan(1);
  setAirBaseImpulse(300); // 300 seconds, 5 min
  setAirairStreamIntensity(60); //percents

  // init and turn off relays
  pinMode(PIN_HEATER_RELAY, OUTPUT);
  pinMode(PIN_FAN_RELAY, OUTPUT);
  setStateHeater(0);
  setStateFan(0);

  // first initialization EEPROM
  if (EEPROM.read(VER_MEM_ADDR) != versionMem) {
    EEPROM.write(VER_MEM_ADDR, versionMem);
    saveDataStateToMem();
  }

  // read from EEPROM
  restoreDataStateFromMem();
  
  calcFanPeriods();

  // OneWire 
  // precision of temperature sensor
  ds.setResolution(9); 

  // executor for fetch temperature
  timerFetchTemperature.setTimeout(pollTemperatureSensor);
  timerFetchTemperature.restart();
  
  // executor for turn on/off fan
  timerFanControl.setTimeout(getAirFanTimerPeriod());
  timerFanControl.restart();
  timerFreezeFanControl.setTimeout(1000);
  timerFreezeFanControl.restart();
  
  timerSaveToMemmory.setTimeout(60000);
  timerSaveToMemmory.restart();

  // rs485
  modbus.addWordArea(VIRTUAL_ADDRESS, _D, DATA_SIZE);
  //modbus.begin(WD.modbusID, WD.modbusSpeed);
  modbus.begin(11, 9600);
}
 
void loop() {
  if(timerFetchTemperature.onRestart()) {  
    handlerFetchTemperature();
    handlerHeater();
  }
  if(timerFanControl.onRestart()) {
    handlerFan();
  }
  if(timerFreezeFanControl.onRestart()) {
    handlerFreezeFan();
  }
  if(timerSaveToMemmory.onRestart()) {
    if (!cmpArr(_D, _DC, DATA_SIZE)) {
      saveDataStateToMem();
    }
  }

  copyArr(_D, _DC, DATA_SIZE);
  
  // waiting for requests from master
  // reading and writing _D according to requests from master happens here
  modbus.process();

  if (!cmpArr(_D, _DC, DATA_SIZE)) {
    copyDataFromModbusRegToStruct();
    // saveDataStateToMem();
  }
  
  delay(50);
}

void handlerFetchTemperature() {
  if (ds.selectNext()) {
    setAirCurrentTemp(round(ds.getTempC() * 10));
  } 
}

bool cmpf(float A, float B, float epsilon = 0.005f) {
  return (fabs(A - B) < epsilon);
}
void copyArr(u16 arr1[], u16 arr2[], int size) {
    for (int i = 0; i < size; ++i) {
        arr2[i] = arr1[i];
    }
}
bool cmpArr(u16 arr1[], u16 arr2[], int size) {
    for (int i = 0; i < size; ++i) {
        if (arr1[i] != arr2[i]) {
            return false;
        }
    }
    return true;
}

void handlerFreezeFan() {
  Serial.print("Temperature: curr-");
  Serial.print(WD.airCurrentTemp);
  Serial.print(", desire-");
  Serial.println(WD.airDesirableTemp);
  // Serial.print("gisterezisAirTemp: ");
  // Serial.println(WD.gisterezisAirTemp);
  Serial.print("Stream: intens-");
  Serial.print(WD.airIntensity);
  Serial.print(", fan-");
  Serial.print(WD.enabledFan);
  Serial.print(", heater-");
  Serial.println(WD.enabledHeater);
  // Serial.print("Fan Work Time = ");
  // Serial.println(fanWorkTime);
  // Serial.print("Fan Pause Time = ");
  // Serial.println(fanPauseTime);
  // Serial.print("Fan = ");
  // Serial.println(WD.stateFan);
  // Serial.print("freeze Fan = ");
  // Serial.println(WD.periodFreezeFan);
  // Serial.print("Heater = ");
  // Serial.println(WD.stateHeater);
  // Serial.println();

  if (WD.enabledFan) {
    if (WD.periodFreezeFan > 0) {
        setStateFan(1);
        WD.periodFreezeFan--;
        if (WD.periodFreezeFan == 0) _D[I_PERIOD_FREEZE] = 0;
    }
    if (WD.periodFreezeFan < 0) {
        setStateFan(0);
        WD.periodFreezeFan++;
        if (WD.periodFreezeFan == 0) _D[I_PERIOD_FREEZE] = 0;
    }
  } else {
    setStateFan(0);
  }

  if (!WD.enabledHeater) {
    setStateHeater(0);
  }
}
void handlerFan() {
  if (WD.enabledFan) {
    if (WD.periodFreezeFan == 0) {
      calcFanPeriods();
      
      timerFanControl.setTimeout(getAirFanTimerPeriod());
      timerFanControl.restart();
        
      if (WD.stateFan == 0) {
        setStateFan(1);
      } else {
        if (fanPauseTime > 0) { // intensity < 100%
          setStateFan(0);
        }
      }
    }
  } else {
    WD.periodFreezeFan = 0;
    setStateFan(0);
  }
}

/**
 * Sets Hi/Lo value of pin accordingly temperature of air
 */
void handlerHeater() {
  if (WD.enabledHeater && WD.enabledFan) { // heater mustn't work w/o fan !!!!
    float topT = WD.airDesirableTemp + WD.gisterezisAirTemp;
    float lowT = WD.airDesirableTemp - WD.gisterezisAirTemp;
    if (topT < WD.airCurrentTemp || cmpf(topT, WD.airCurrentTemp)) {
      setStateHeater(0); 
    }
    if (lowT > WD.airCurrentTemp || cmpf(lowT, WD.airCurrentTemp)) {
      setStateHeater(1);
    }
  } else {
    setStateHeater(0);
  }
}

void copyDataFromModbusRegToStruct() {
  if (_D[I_MODBUS_DEVICE_ID] != _DC[I_MODBUS_DEVICE_ID]) WD.modbusID = _D[I_MODBUS_DEVICE_ID];
  if (_D[I_MODBUS_SPEED] != _DC[I_MODBUS_SPEED]) WD.modbusSpeed = _D[I_MODBUS_SPEED];
  if (_D[I_ENABLED_FAN] != _DC[I_ENABLED_FAN]) WD.enabledFan = _D[I_ENABLED_FAN];
  if (_D[I_ENABLED_HEATER] != _DC[I_ENABLED_HEATER]) WD.enabledHeater = _D[I_ENABLED_HEATER];
  if (_D[I_AIR_DESIRABLE_TEMP] != _DC[I_AIR_DESIRABLE_TEMP]) WD.airDesirableTemp = _D[I_AIR_DESIRABLE_TEMP];
  if (_D[I_AIR_TEMP_GISTEREZIS] != _DC[I_AIR_TEMP_GISTEREZIS]) WD.gisterezisAirTemp = _D[I_AIR_TEMP_GISTEREZIS];
  if (_D[I_AIR_INTENSITY] != _DC[I_AIR_INTENSITY]) WD.airIntensity = _D[I_AIR_INTENSITY];
  if (_D[I_AIR_BASE_IMPULSE] != _DC[I_AIR_BASE_IMPULSE]) WD.airBaseImpulse = _D[I_AIR_BASE_IMPULSE];
  if (_D[I_PERIOD_FREEZE] != _DC[I_PERIOD_FREEZE]) WD.periodFreezeFan = _D[I_PERIOD_FREEZE];
}

// temp is *10
void setGisterezisAirTemp(long temp) {
  WD.gisterezisAirTemp = temp;
  _D[I_AIR_TEMP_GISTEREZIS] = temp & 0xFFFF;
}
void setAirDesirableTemp(int temp) {
  WD.airDesirableTemp = temp;
  _D[I_AIR_DESIRABLE_TEMP] = temp & 0xFFFF;
}
void setAirCurrentTemp(long temp) {
  if (temp>800) return; //it's failure in temperature detection
  WD.airCurrentTemp = temp;
  _D[I_AIR_CURRENT_TEMP] = temp & 0xFFFF;
}

void setAirairStreamIntensity(long value) {
  if (value < 10) {
    value = 10;
  }
  if (value > 100) {
    value = 100;
  }
  WD.airIntensity = value;
  _D[I_AIR_INTENSITY] = value & 0xFFFF;
}
void setAirBaseImpulse(long value) {
  WD.airBaseImpulse = value;
  _D[I_AIR_BASE_IMPULSE] = value & 0xFFFF;
}
void setStateHeater(int value) {
  if (WD.stateFan == 1 && value == 1) {
    digitalWrite(PIN_HEATER_RELAY, LOW);
    _D[I_STATE_HEATER] = WD.stateHeater = 1;  
  } else {
    digitalWrite(PIN_HEATER_RELAY, HIGH);
    _D[I_STATE_HEATER] = WD.stateHeater = 0;
  }
}
void setStateFan(int value) {
  digitalWrite(PIN_FAN_RELAY, value == 1 ? LOW: HIGH);
  _D[I_STATE_FAN] = WD.stateFan = value;

  handlerHeater();
}
void setEnabledHeater(int value) {
  _D[I_ENABLED_HEATER] = WD.enabledHeater = value;
}
void setEnabledFan(int value) {
  _D[I_ENABLED_FAN] = WD.enabledFan = value;
}
void setModbusDeviceId(int value) {
  _D[I_MODBUS_DEVICE_ID] = WD.modbusID = value;
}
void setModbusSpeed(int value) {
  _D[I_MODBUS_SPEED] = WD.modbusSpeed = value;
}


void calcFanPeriods() {
  if (WD.airIntensity <= 50) {
    fanWorkTime = WD.airBaseImpulse;
    if (WD.airIntensity == 0) {
      fanPauseTime = WD.airBaseImpulse; // one tact
    } else {
      fanPauseTime = round(WD.airBaseImpulse * 100 / WD.airIntensity - WD.airBaseImpulse);
    }
  } else {
    if (WD.airIntensity == 100) {
      fanWorkTime = round(WD.airBaseImpulse * 100); // ~8.33 hours
    } else {
      fanWorkTime = round(WD.airBaseImpulse * 100 / (100-WD.airIntensity) - WD.airBaseImpulse);
    }
    fanPauseTime = WD.airBaseImpulse;
  }
  // Serial.print("calc fanWorkTime = ");
  // Serial.println(fanWorkTime);
  // Serial.print("calc fanPauseTime = ");
  // Serial.println(fanPauseTime);
}
long getAirFanTimerPeriod() {
  // Serial.print("fanWorkTime = ");
  // Serial.println(fanWorkTime);
  // Serial.print("fanPauseTime = ");
  // Serial.println(fanPauseTime);
  long p = (WD.stateFan == 0 ? fanWorkTime : fanPauseTime) * 1000L;
  // Serial.print("AirFanTimerPeriod = ");
  // Serial.println(p);
  return p;
}
void saveDataStateToMem() {
  Serial.println("+++ write data to memory");
  MemD.modbusID = WD.modbusID;
  MemD.airDesirableTemp = WD.airDesirableTemp;
  MemD.gisterezisAirTemp = WD.gisterezisAirTemp;
  MemD.airIntensity = WD.airIntensity;
  MemD.airBaseImpulse = WD.airBaseImpulse;
  MemD.enabledFan = WD.enabledFan;
  MemD.enabledHeater = WD.enabledHeater;
  MemD.restoreStateFromMem = WD.restoreStateFromMem;
  EEPROM.put(DTA_MEM_ADDR, MemD);
}
void restoreDataStateFromMem() {
  EEPROM.get(DTA_MEM_ADDR, MemD);
  WD.modbusID = MemD.modbusID;
  WD.airDesirableTemp = MemD.airDesirableTemp;
  WD.gisterezisAirTemp = MemD.gisterezisAirTemp;
  WD.airIntensity = MemD.airIntensity;
  WD.airBaseImpulse = MemD.airBaseImpulse;
  WD.enabledFan = MemD.enabledFan;
  WD.enabledHeater = MemD.enabledHeater;
  WD.restoreStateFromMem = MemD.restoreStateFromMem;
}